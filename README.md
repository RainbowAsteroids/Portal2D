# Portal2D
Portal, but minus a dimension and story

This is a simple game I've been making on and off for some time now in Godot. If you want the latest version, check it out [here](https://mega.nz/folder/HdxB0ZTS#lHFn_KhyYT1On3dZaaKjwg).

Any bug reports or suggestions are welcome on the issues tab and any feedback is welcome at my email at feedback@portal2d.xyz
